#!/usr/bin/python
import random
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from subprocess import Popen, PIPE


def pwgen(pw_length=12, alphabet="abcdefghijkmnopqrstuvwxyz3479ACEFHJKLMNPRTUVWXY"):
    mypw = ""
    for i in range(pw_length):
        next_index = random.randrange(len(alphabet))
        mypw += alphabet[next_index]
    return mypw


def send_mail(sysmail, txt, logid, has_errors=False):
    if has_errors:
        with_result = "failed"
    else:
        with_result = "successful"
    html = MIMEText(txt)
    msg = MIMEMultipart("alternative")
    sysmail = "eastern@amhost.net" if sysmail == "" else sysmail
    msg["From"] = "maya"
    msg["To"] = sysmail
    msg["Subject"] = "bZzzz, setup # %s %s" % (logid, with_result)
    msg.attach(html)
    p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
    p.communicate(msg.as_string())
