#!/usr/bin/python
from www import db


class Images(db.Model):
    imgz_id = db.Column(db.Integer, primary_key=True)
    imgz_name = db.Column(db.String(32), index=True)
    imgz_uniq = db.Column(db.String(32), index=True, unique=True)
    imgz_ip = db.Column(db.String(32), index=True)
    imgz_note = db.Column(db.String(256), index=True)
    imgz_crtd = db.Column(db.DateTime, default=db.func.now())
    imgz_uptd = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())
    imgz_views = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '<Images %r>' % self.imgz_name
