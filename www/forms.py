#!/usr/bin/python
from flask_wtf import Form, RecaptchaField
from wtforms import BooleanField, StringField, FileField, SubmitField
from wtforms.validators import DataRequired


class LoginForm(Form):
    openid = StringField('openid', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)


class UploadImgForm(Form):
    reqfile = FileField()
    recaptcha = RecaptchaField()
    submit = SubmitField('Submit')
