#!/usr/bin/python
# -*- coding: utf-8 -*-
# http://marshmallow.readthedocs.org/en/latest/examples.html
import os
import time
import hashlib

from flask import jsonify, make_response, render_template, abort, redirect
from flask import request
from flask import flash
from flask import send_from_directory
from sqlalchemy.exc import IntegrityError

from www import app
from www import db
from www.functions import pwgen
from .models import Images
from .schemas import ImagesSchema
from .forms import UploadImgForm

from werkzeug.utils import secure_filename

SECRET_KEY = app.config['SECRET_KEY']
UPLOAD_FOLDER = app.config['UPLOAD_FOLDER']
STATIC_FOLDER = app.config['STATIC_FOLDER']


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(STATIC_FOLDER, 'favicon.ico', mimetype='image/x-icon')


@app.route('/static/<string:filename>')
def return_static(filename):
    return send_from_directory(STATIC_FOLDER, filename)


# noinspection PyUnusedLocal
@app.errorhandler(400)
def not_found400(error):
    return make_response(jsonify({'error': 'Bad request. 400'}), 400)


# noinspection PyUnusedLocal
@app.errorhandler(404)
def not_found404(error):
    return make_response(jsonify({'error': 'Not found. 404'}), 404)


@app.route('/')
@app.route('/index.php')
def index():
    form = UploadImgForm()
    return render_template("index.html", form=form, remote_addr=request.remote_addr, url_root=request.url_root)


@app.route('/api/imgz/', methods=['GET'])
def get_imgz():
    imgz = Images.query.all()
    if imgz is None:
        return jsonify({"message": "Images could not be found."}), 404
    serializer = ImagesSchema(many=True, only=('imgz_id', 'imgz_crtd', 'imgz_uniq', 'imgz_views'))
    result = serializer.dump(imgz)
    return jsonify({'data': result.data})


@app.route('/api/imgz/<int:imgz_id>', methods=['GET'])
def get_img(imgz_id):
    imgz = Images.query.get(imgz_id)
    if imgz is None:
        return jsonify({"message": "Images could not be found."}), 404
    return jsonify({'data': ImagesSchema().dump(imgz).data})


@app.route('/api/imgz/', methods=['POST'])
def create_imgz():
    form = UploadImgForm()
    key_validation = False
    if 'key' in request.form and request.form["key"] == '12ea5e932124142c5ef3c8d5a02557de':
        key_validation = True

    if form.validate_on_submit() or key_validation:

        if 'reqfile' not in request.files:
            return jsonify({'result': "No file part"})
        reqfile = request.files['reqfile']

        if reqfile.filename == '':
            return jsonify({'result': "No selected file"})

        if reqfile and allowed_file(reqfile.filename):

            uniq = pwgen()
            filename = "%s_%s" % (uniq, secure_filename(reqfile.filename))
            reqfile.save(os.path.join(UPLOAD_FOLDER, filename))
            try:
                new_imgz = Images(
                    imgz_ip=request.remote_addr,
                    imgz_name=secure_filename(reqfile.filename),
                    imgz_uniq=uniq,
                    imgz_note='for info, filename=' + filename
                )
                db.session.add(new_imgz)
                db.session.commit()
                db.session.refresh(new_imgz)
            except IntegrityError:
                return jsonify({"message": "IntegrityError. Bad request"}), 400
        else:
            return jsonify({'result': "Bad filename"}), 200

        if key_validation:
            return jsonify({'data': ImagesSchema().dump(new_imgz).data}), 201
        else:
            return redirect("%sz/%s" % (request.url_root, uniq), code=302)
    else:
        flash('reCaptcha validation error, try again...', 'error')
        return redirect(request.url_root, code=301)


@app.route('/z/<string:uniq>', methods=['GET'])
def show_imgz(uniq):
    imgz = Images.query.filter_by(imgz_uniq=uniq).first()
    if imgz is None:
        return jsonify({"message": "Images could not be found."}), 404
    ttl_url = str(int(time.time()) + 3600)
    string = '%s%s%s%s_%s' % (SECRET_KEY, request.remote_addr, ttl_url, uniq, imgz.imgz_name)
    md5hash = hashlib.md5(string).hexdigest()
    img_url = '%ss/%s,%s/%s_%s' % (request.url_root, md5hash, ttl_url, uniq, imgz.imgz_name)
    try:
        imgz.imgz_views += 1
        db.session.commit()
    except Exception as error:
        print('caught this error: ' + repr(error))
    return render_template("show_imgz.html", img_url=img_url, url_root=request.url_root)


@app.route('/premium/<string:filename>')
def show_img_premium(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)


@app.route('/s/<string:md5hash_url>,<int:ttl_url>/<string:filename>')
def show_img(md5hash_url, ttl_url, filename):
    if ttl_url < time.time():
        abort(404)
    string = SECRET_KEY + request.remote_addr + str(ttl_url) + filename
    md5hash = hashlib.md5(string).hexdigest()
    if md5hash == md5hash_url:
        return send_from_directory(UPLOAD_FOLDER, filename)
    else:
        abort(404)
