#!/usr/bin/python
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, static_url_path="")
app.debug = True
app.config.from_object('config')
db = SQLAlchemy(app)
# noinspection PyPep8
from www import views, models
db.create_all()