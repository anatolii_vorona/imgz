#!/usr/bin/python
from marshmallow import Schema, fields


class ImagesSchema(Schema):
    formated_info = fields.Method("format_info")

    @staticmethod
    def format_info(custom):
        return "{}, {}, {}, {}".format(custom.imgz_note, custom.imgz_name, custom.imgz_crtd, custom.imzg_uptd)

    class Meta:
        fields = ('imgz_id', 'imgz_ip', 'imgz_views', 'imgz_uniq', 'imgz_note', 'imgz_name', 'imgz_crtd', 'imgz_uptd',
                  'formated_info')
