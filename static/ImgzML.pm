#! /usr/bin/env perl
###################################################
#
#  Copyright (C) 2016 AV <imgz@imgz.ml>
#
#  This file contain upload plugin for Shutter.
#
#  http://shutter-project.org/contribute/developers/
#
###################################################
 
package ImgzML;
 
use lib $ENV{'SHUTTER_ROOT'}.'/share/shutter/resources/modules';
 
use utf8;
use strict;
use POSIX qw/setlocale/;
use Locale::gettext;
use Glib qw/TRUE FALSE/;
use Data::Dumper;
 
use Shutter::Upload::Shared;
our @ISA = qw(Shutter::Upload::Shared);
 
my $d = Locale::gettext->domain("shutter-plugins");
$d->dir( $ENV{'SHUTTER_INTL'} );
 
my %upload_plugin_info = (
    'module'                        => "ImgzML",
    'url'                           => "https://imgz.ml/",
    'registration'                  => "https://imgz.ml/register",
    'name'                          => "Imgz.ML",
    'description'                   => "Upload screenshots to Imgz.ML",
    'supports_anonymous_upload'     => TRUE,
    'supports_authorized_upload'    => FALSE,
    'supports_oauth_upload'         => FALSE,
);
 
binmode( STDOUT, ":utf8" );
if ( exists $upload_plugin_info{$ARGV[ 0 ]} ) {
    print $upload_plugin_info{$ARGV[ 0 ]};
    exit;
}
 
 
#don't touch this
sub new {
    my $class = shift;
 
    #call constructor of super class (host, debug_cparam, shutter_root, gettext_object, main_gtk_window, ua)
    my $self = $class->SUPER::new( shift, shift, shift, shift, shift, shift );
 
    bless $self, $class;
    return $self;
}
 
#load some custom modules here (or do other custom stuff)   
sub init {
    my $self = shift;
 
    use JSON;
    use LWP::UserAgent;
    use HTTP::Request::Common;
     
    return TRUE;    
}
 
#handle 
sub upload {
    my ( $self, $upload_filename, $username, $password ) = @_;
 
    #store as object vars
    $self->{_filename} = $upload_filename;
    $self->{_username} = $username;
    $self->{_password} = $password;
 
    utf8::encode $upload_filename;
    utf8::encode $password;
    utf8::encode $username;
 
    #examples related to the sub 'init'
    my $json_coder = JSON::XS->new;
 
    my $browser = LWP::UserAgent->new(
        'timeout'    => 20,
        'keep_alive' => 10,
        'env_proxy'  => 1,
    );
     
    eval{
        my $json = JSON->new();
        my %params = (
                        'reqfile' => [$upload_filename],
                        'key'   => '12ea5e932124142c5ef3c8d5a02557de',
                    );
        my @params = (  "https://imgz.ml/api/imgz/",
                        'Content_Type' => 'multipart/form-data',
                        'Content' => [%params]
                    );

        my $req = HTTP::Request::Common::POST(@params);
        my $rsp = $browser->request($req);
        if ( ! $rsp->is_success ) {
            die $rsp->status_line;
        }

        my $json_content = $json->decode( $rsp->content );
        my $status_line = $rsp->status_line;
        my $status_code = $rsp->code;
        my $uniq = $json_content->{'data'}->{'imgz_uniq'};
        $self->{_links}->{$status_line} = $upload_plugin_info{'url'}.'z/'.$uniq;
        $self->{_links}{'status'} = $status_code;
#        print Dumper $self->{_links};
    };
    if($@){
        $self->{_links}{'status'} = $@;
        return %{ $self->{_links} };
    }
    if($self->{_links}{'status'} == 999){
        return %{ $self->{_links} };
    }
    return %{ $self->{_links} };
}
 
1;
